////Copyright (c) 2017. 章钦豪. All rights reserved.
//package com.kunfei.bookshelf.view.activity;
//
//import android.Manifest;
//import android.animation.Animator;
//import android.animation.ValueAnimator;
//import android.annotation.SuppressLint;
//import android.app.Activity;
//import android.app.Instrumentation;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.content.pm.PackageManager;
//import android.graphics.Rect;
//import android.net.Uri;
//import android.os.Build;
//import android.os.Bundle;
//import android.os.SystemClock;
//import android.provider.Settings;
//import android.util.Log;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.Window;
//import android.view.WindowManager;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.google.gson.JsonElement;
//import com.google.gson.JsonObject;
//import com.google.gson.JsonParser;
//import com.kunfei.bookshelf.MApplication;
//import com.kunfei.bookshelf.R;
//import com.kunfei.bookshelf.base.BaseModelImpl;
//import com.kunfei.bookshelf.base.observer.MyObserver;
//import com.kunfei.bookshelf.bean.BookSourceBean;
//import com.kunfei.bookshelf.constant.AppConstant;
//import com.kunfei.bookshelf.model.BookSourceManager;
//import com.kunfei.bookshelf.model.analyzeRule.AnalyzeHeaders;
//import com.kunfei.bookshelf.model.impl.IHttpGetApi;
//import com.kunfei.bookshelf.presenter.ReadBookPresenter;
//import com.kunfei.bookshelf.utils.RxUtils;
//import com.prujwk.jdyphn.ww.sdk.Interfaces.RDInterface;
//import com.prujwk.jdyphn.ww.sdk.rds.SplashView;
//import com.smarx.notchlib.NotchScreenManager;
//
//import java.io.BufferedReader;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//import java.io.UnsupportedEncodingException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Random;
//
//import androidx.annotation.Nullable;
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.core.app.ActivityCompat;
//import androidx.core.content.ContextCompat;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import io.reactivex.Observable;
//import retrofit2.Response;
//
//import static com.kunfei.bookshelf.constant.AppConstant.KEY_AD_INTERVALS_TIME;
//import static com.kunfei.bookshelf.constant.AppConstant.KEY_OPEN_AD;
//
//public class NewWelcomeActivity extends AppCompatActivity {
//    public boolean bAllowJump;
//
//    public String[] permissions = null;
//    public final SharedPreferences preferences = MApplication.getConfigPreferences();
//
//    private PermissionListener permissionListener = new PermissionListener() {
//        @Override
//        public void onGranted(List<String> grantedPermission) {
//            this.onGranted();
//        }
//
//        @Override
//        public void onGranted() {
//            requestSpreadAd();
//        }
//
//        @Override
//        public void onDenied(List<String> deniedPermission) {
//            Toast.makeText(NewWelcomeActivity.this,
//                    deniedPermission.get(0) + "权限被拒绝了", Toast.LENGTH_SHORT).show();
//            finish();
//        }
//    };
//
//    @BindView(R.id.iv_bg)
//    ImageView ivBg;
//    @BindView(R.id.tvJump)
//    TextView mTvJump;
//    @BindView(R.id.rlBottom)
//    RelativeLayout mLLBottom;
//    @BindView(R.id.spreadlayout)
//    RelativeLayout mRLSpreadlayout;
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        //全屏
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        // 避免从桌面启动程序后，会重新实例化入口类的activity
//        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
//            finish();
//            return;
//        }
//        setContentView(R.layout.activity_welcome);
//        bAllowJump = getIntent().getBooleanExtra("AllowJump", true);
//        NotchScreenManager.getInstance().setDisplayInNotch(NewWelcomeActivity.this);
//        ButterKnife.bind(this);
//        permissions = new String[]{
//                Manifest.permission.READ_EXTERNAL_STORAGE,
//                Manifest.permission.WRITE_EXTERNAL_STORAGE,
//                Manifest.permission.ACCESS_FINE_LOCATION,
//                Manifest.permission.ACCESS_COARSE_LOCATION,
//                Manifest.permission.READ_PHONE_STATE};
//        if (Build.VERSION.SDK_INT >= 23) {
//            requestRunTimePermission(permissions, permissionListener);
//        } else {
//            requestSpreadAd();
//        }
//        initData();
//        initListener();
//    }
//
//    private void initListener() {
//        mTvJump.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                getAllViews(NewWelcomeActivity.this);
//            }
//        });
//    }
//
//    private void doWelcomeAnimation() {
//        findViewById(R.id.spreadlayout).setVisibility(View.GONE);
//        ValueAnimator welAnimator = ValueAnimator.ofFloat(1f, 0f).setDuration(1000);
//        welAnimator.setStartDelay(500);
//        welAnimator.addUpdateListener(animation -> {
//            float alpha = (Float) animation.getAnimatedValue();
//            ivBg.setAlpha(alpha);
//        });
//        welAnimator.addListener(new Animator.AnimatorListener() {
//            @Override
//            public void onAnimationStart(Animator animation) {
//                openActivity();
//            }
//
//            @Override
//            public void onAnimationEnd(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationCancel(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationRepeat(Animator animation) {
//
//            }
//        });
//        welAnimator.start();
//    }
//
//    private void openActivity() {
//        if (bAllowJump) {
//            if (preferences.getBoolean(getString(R.string.pk_default_read), false)) {
//                startReadActivity();
//            } else {
//                startBookshelfActivity();
//            }
//        }
//        finish();
//    }
//
//    private void getAllViews(Activity act) {
//        List<View> list = getAllChildViews(act.getWindow().getDecorView());
//        for (int i=0;i<list.size();i++) {
//            View view = list.get(i);
//            Rect rect = new Rect();
//            view.getGlobalVisibleRect(rect);
//            if (rect.top != 0 && rect.left != 0 && rect.right != 0 && rect.bottom != 0) {
//                if (rect.top < 400 && rect.bottom - rect.top < 400) {
//                    int x = rect.left + new Random().nextInt((rect.right - rect.left));
//                    int y = rect.top + new Random().nextInt((rect.bottom - rect.top));
//                    setMouseClick(x, y);
//                    break;
//                }
//            }
//        }
//    }
//
//    public void setMouseClick(int x, int y) {
//        MotionEvent evenDownt = MotionEvent.obtain(System.currentTimeMillis(),
//                System.currentTimeMillis() + 100, MotionEvent.ACTION_DOWN, x, y, 0);
//        dispatchTouchEvent(evenDownt);
//        MotionEvent eventUp = MotionEvent.obtain(System.currentTimeMillis(),
//                System.currentTimeMillis() + 100, MotionEvent.ACTION_UP, x, y, 0);
//        dispatchTouchEvent(eventUp);
//        evenDownt.recycle();
//        eventUp.recycle();
//    }
//
//
//    private List<View> getAllChildViews(View view) {
//        List<View> allchildren = new ArrayList<View>();
//        if (view instanceof ViewGroup) {
//            ViewGroup vp = (ViewGroup) view;
//            for (int i = 0; i < vp.getChildCount(); i++) {
//                View viewchild = vp.getChildAt(i);
//                allchildren.add(viewchild);
//                allchildren.addAll(getAllChildViews(viewchild));
//            }
//        }
//        return allchildren;
//    }
//
//    private void requestSpreadAd() {
//        if (preferences.getBoolean(KEY_OPEN_AD, true)) {
//            SplashView splashView = new SplashView();
//            splashView.setInterface(this, new RDInterface() {
//                @Override
//                public void onShow() {
//                    super.onShow();
//                }
//
//                @Override
//                public void onClose() {
//                    super.onClose();
//                    openActivity();
//                }
//
//                @Override
//                public void onClick() {
//                    super.onClick();
//                    openActivity();
//                }
//
//                @Override
//                public void rdView(ViewGroup view) {
//                    super.rdView(view);
//                    LinearLayout.LayoutParams params = new LinearLayout
//                            .LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
//                            ViewGroup.LayoutParams.MATCH_PARENT);
//                    view.setLayoutParams(params);
//                    mRLSpreadlayout.addView(view);
//                    mLLBottom.setVisibility(View.VISIBLE);
//                }
//
//                @Override
//                public void onLoadSuccess() {
//                    super.onLoadSuccess();
//                    splashView.show();
//                }
//
//                @Override
//                public void onError(String s) {
//                    openActivity();
//                    super.onError(s);
//                }
//            });
//            splashView.load();
//        } else {
//            doWelcomeAnimation();
//        }
//    }
//
//    private void startBookshelfActivity() {
//        startActivity(new Intent(this, MainActivity.class));
//        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//    }
//
//    private void startReadActivity() {
//        Intent intent = new Intent(this, ReadBookActivity.class);
//        intent.putExtra("openFrom", ReadBookPresenter.OPEN_FROM_APP);
//        startActivity(intent);
//    }
//
//    public void initData() {
//        try {
//            InputStream inputStream = getAssets().open("myBookSource.json");
//            InputStreamReader inputStreamReader = null;
//            try {
//                inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
//            } catch (UnsupportedEncodingException e1) {
//                e1.printStackTrace();
//            }
//            BufferedReader reader = new BufferedReader(inputStreamReader);
//            StringBuffer sb = new StringBuffer("");
//            String line;
//            while ((line = reader.readLine()) != null) {
//                sb.append(line);
//            }
//            Observable<List<BookSourceBean>> observable = BookSourceManager.importSource(sb.toString());
//            if (observable != null) {
//                observable.subscribe(new MyObserver<List<BookSourceBean>>() {
//                    @SuppressLint("DefaultLocale")
//                    @Override
//                    public void onNext(List<BookSourceBean> bookSourceBeans) {
//                        Log.d("#DEBUG", "ImportSource Successed!");
//                        preferences.edit()
//                                .putBoolean("importDefaultBookSource",
//                                        bookSourceBeans.size() > 0 ? true : false)
//                                .apply();
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        Log.d("#DEBUG", "ImportSource Failed!");
//                        preferences.edit()
//                                .putBoolean("importDefaultBookSource", false)
//                                .apply();
//                    }
//                });
//            }
//
//            BaseModelImpl.getInstance().getRetrofitString("https://gitee.com")
//                    .create(IHttpGetApi.class)
//                    .get("https://gitee.com/laishoifh/Reader/raw/light_reader/%E9%85%8D%E7%BD%AE%E6%96%87%E4%BB%B6/config.json",
//                            AnalyzeHeaders.getMap(null))
//                    .compose(RxUtils::toSimpleSingle)
//                    .subscribe(new MyObserver<Response<String>>() {
//                        @Override
//                        public void onNext(Response<String> response) {
//                            if (response.isSuccessful()) {
//                                String result = response.body();
//                                JsonObject jsonObject = new JsonParser().parse(result).getAsJsonObject();
//
//                                {
//                                    JsonElement jsonElement = jsonObject.get("allowOpenApp");
//                                    if (jsonElement != null) {
//                                        boolean allowOpenApp = jsonElement.getAsBoolean();
//                                        preferences.edit()
//                                                .putBoolean(AppConstant.KEY_ALLOWOPENAPP, allowOpenApp)
//                                                .apply();
//                                    }
//                                }
//                                {
//                                    JsonElement jsonElement = jsonObject.get("gitee");
//                                    if (jsonElement != null) {
//                                        String sGiteeURl = jsonObject.get("gitee").getAsString();
//                                        preferences.edit()
//                                                .putString(AppConstant.KEY_GITEE_URL, sGiteeURl)
//                                                .apply();
//                                    }
//                                }
//
//                                {
//                                    JsonElement jsonElement = jsonObject.get("downloadUrl");
//                                    if (jsonElement != null) {
//                                        String sDownloadURL = jsonObject.get("downloadUrl").getAsString();
//                                        preferences.edit()
//                                                .putString(AppConstant.KEY_APK_DOWNLOADURL, sDownloadURL)
//                                                .apply();
//                                    }
//                                }
//                                {
//                                    JsonElement jsonElement = jsonObject.get("join_qq_group_key");
//                                    String sJoinQQGroupKey = jsonElement.getAsString();
//                                    preferences.edit()
//                                            .putString(AppConstant.KEY_JOIN_QQ_GROUP, sJoinQQGroupKey)
//                                            .apply();
//                                }
//
//                                {
//                                    JsonElement jsonElement = jsonObject.get("openAD");
//                                    if (jsonElement != null) {
//                                        boolean bOpenAD = jsonElement.getAsBoolean();
//                                        preferences.edit()
//                                                .putBoolean(KEY_OPEN_AD, bOpenAD)
//                                                .apply();
//                                    }
//                                }
//
//                                {
//                                    JsonElement jsonElement = jsonObject.get("ad_intervals_time");
//                                    if (jsonElement != null) {
//                                        long lAdIntervalsTime = jsonElement.getAsLong();
//                                        preferences.edit()
//                                                .putLong(KEY_AD_INTERVALS_TIME, lAdIntervalsTime)
//                                                .apply();
//                                    }
//                                }
//                            }
//                        }
//                    });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    protected void requestRunTimePermission(String[] permissions,
//                                            PermissionListener listener) {
//        // 用于存放为授权的权限
//        List<String> permissionList = new ArrayList<>();
//        for (String permission : permissions) {
//            // 判断是否已经授权，未授权，则加入待授权的权限集合中
//            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
//                permissionList.add(permission);
//            }
//        }
//
//        // 判断集合
//        if (!permissionList.isEmpty()) { // 如果集合不为空，则需要去授权
//            ActivityCompat.requestPermissions(this,
//                    permissionList.toArray(new String[permissionList.size()]),
//                    1);
//        } else { // 为空，则已经全部授权
//            listener.onGranted();
//        }
//    }
//
//    public interface PermissionListener {
//
//        /**
//         * 授权成功
//         */
//        void onGranted();
//
//        /**
//         * 授权部分
//         *
//         * @param grantedPermission
//         */
//        void onGranted(List<String> grantedPermission);
//
//        /**
//         * 拒绝授权
//         *
//         * @param deniedPermission
//         */
//        void onDenied(List<String> deniedPermission);
//    }
//
//    /**
//     * 权限申请结果
//     *
//     * @param requestCode  请求码
//     * @param permissions  所有的权限集合
//     * @param grantResults 授权结果集合
//     */
//    @Override
//    public void onRequestPermissionsResult(int requestCode,
//                                           String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        switch (requestCode) {
//            case 1:
//                if (grantResults.length > 0) {
//                    // 被用户拒绝的权限集合
//                    List<String> deniedPermissions = new ArrayList<>();
//                    // 用户通过的权限集合
//                    List<String> grantedPermissions = new ArrayList<>();
//                    for (int i = 0; i < grantResults.length; i++) {
//                        // 获取授权结果，这是一个int类型的值
//                        int grantResult = grantResults[i];
//
//                        if (grantResult != PackageManager.PERMISSION_GRANTED) { // 用户拒绝授权的权限
//                            String permission = permissions[i];
//                            deniedPermissions.add(permission);
//                        } else { // 用户同意的权限
//                            String permission = permissions[i];
//                            grantedPermissions.add(permission);
//                        }
//                    }
//
//                    if (deniedPermissions.isEmpty()) { // 用户拒绝权限为空
//                        permissionListener.onGranted();
//                    } else { // 不为空
//                        Toast.makeText(this, "应用缺少必要的权限！请点击\"权限\"，打开所需要的权限。", Toast.LENGTH_LONG).show();
//                        // 回调授权成功的接口
//                        permissionListener.onDenied(deniedPermissions);
//                        // 回调授权失败的接口
//                        permissionListener.onGranted(grantedPermissions);
//                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//                        intent.setData(Uri.parse("package:" + getPackageName()));
//                        startActivity(intent);
//                    }
//                }
//                break;
//            default:
//                break;
//        }
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//    }
//}
