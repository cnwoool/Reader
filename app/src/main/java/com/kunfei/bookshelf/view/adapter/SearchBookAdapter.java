//Copyright (c) 2017. 章钦豪. All rights reserved.
package com.kunfei.bookshelf.view.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kunfei.bookshelf.DbHelper;
import com.kunfei.bookshelf.R;
import com.kunfei.bookshelf.bean.BookKindBean;
import com.kunfei.bookshelf.bean.SearchBookBean;
import com.kunfei.bookshelf.help.GlideUtil;
import com.kunfei.bookshelf.help.Levenshtein;
import com.kunfei.bookshelf.utils.StringUtils;
import com.kunfei.bookshelf.utils.theme.ThemeStore;
import com.kunfei.bookshelf.view.adapter.base.BaseListAdapter;
import com.kunfei.bookshelf.widget.recycler.refresh.RefreshRecyclerViewAdapter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

import static com.kunfei.bookshelf.utils.StringUtils.isTrimEmpty;


public class SearchBookAdapter extends RefreshRecyclerViewAdapter
{
    private WeakReference<Activity> activityRef;
    private List<SearchBookBean> searchBooks;
    private BaseListAdapter.OnItemClickListener itemClickListener;

    public SearchBookAdapter(Activity activity)
    {
        super(true);
        this.activityRef = new WeakReference<>(activity);
        searchBooks = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateIViewHolder(ViewGroup parent, int viewType)
    {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_book, parent, false));
    }


    @SuppressLint("DefaultLocale")
    @Override
    public void onBindIViewHolder(final RecyclerView.ViewHolder holder, final int position)
    {
        SearchBookBean searchBookBean = searchBooks.get(position);
        Activity activity = activityRef.get();
        holder.itemView.setBackgroundColor(ThemeStore.backgroundColor(activity));
        MyViewHolder myViewHolder = (MyViewHolder) holder;
        myViewHolder.flContent.setOnClickListener(v -> {
            if (itemClickListener != null)
                itemClickListener.onItemClick(v, position);
        });
        if (!activity.isFinishing())
        {
            GlideUtil.loadCircleImage(activity, ((MyViewHolder) holder).ivCover, searchBookBean.getCoverUrl());
        }
        myViewHolder.tvName.setText(String.format("%s (%s)", searchBookBean.getName(), searchBookBean.getAuthor()));
        BookKindBean bookKindBean = new BookKindBean(searchBookBean.getKind());
        if (isTrimEmpty(bookKindBean.getKind()))
        {
            myViewHolder.tvKind.setVisibility(View.GONE);
        } else
        {
            myViewHolder.tvKind.setVisibility(View.VISIBLE);
            myViewHolder.tvKind.setText(bookKindBean.getKind());
        }
        if (isTrimEmpty(bookKindBean.getWordsS()))
        {
            myViewHolder.tvWords.setVisibility(View.GONE);
        } else
        {
            myViewHolder.tvWords.setVisibility(View.VISIBLE);
            myViewHolder.tvWords.setText(bookKindBean.getWordsS());
        }
        if (isTrimEmpty(bookKindBean.getState()))
        {
            myViewHolder.tvState.setVisibility(View.GONE);
        } else
        {
            myViewHolder.tvState.setVisibility(View.VISIBLE);
            myViewHolder.tvState.setText(bookKindBean.getState());
        }
        //来源
        if (isTrimEmpty(searchBookBean.getOrigin()))
        {
            myViewHolder.tvOrigin.setVisibility(View.GONE);
        } else
        {
            myViewHolder.tvOrigin.setVisibility(View.VISIBLE);
            myViewHolder.tvOrigin.setText(activity.getString(R.string.origin_format, searchBooks.get(position).getOrigin()));
        }
        //最新章节
        if (isTrimEmpty(searchBookBean.getLastChapter()))
        {
            myViewHolder.tvLasted.setVisibility(View.GONE);
        } else
        {
            myViewHolder.tvLasted.setText(searchBookBean.getLastChapter());
            myViewHolder.tvLasted.setVisibility(View.VISIBLE);
        }
        //简介
        if (isTrimEmpty(searchBookBean.getIntroduce()))
        {
            myViewHolder.tvIntroduce.setVisibility(View.GONE);
        } else
        {
            myViewHolder.tvIntroduce.setText(StringUtils.formatHtml(searchBookBean.getIntroduce()));
            myViewHolder.tvIntroduce.setVisibility(View.VISIBLE);
        }
        if (!TextUtils.isEmpty(searchBookBean.getOrigin()))
            myViewHolder.tvOriginNum.setText(String.format("共%d个源", searchBookBean.getOriginNum()));
    }

    @Override
    public int getIViewType(int position)
    {
        return 0;
    }

    @Override
    public int getICount()
    {
        return searchBooks.size();
    }

    public void setItemClickListener(BaseListAdapter.OnItemClickListener itemClickListener)
    {
        this.itemClickListener = itemClickListener;
    }

    public synchronized void upData(DataAction action, List<SearchBookBean> newDataS)
    {
        switch (action)
        {
            case ADD:
                searchBooks = newDataS;
                notifyDataSetChanged();
                break;
            case CLEAR:
                if (!searchBooks.isEmpty())
                {
                    try
                    {
                        Glide.with(activityRef.get()).onDestroy();
                    } catch (Exception ignored)
                    {
                    }
                    searchBooks.clear();
                    notifyDataSetChanged();
                }
                break;
        }
    }

    public synchronized void addAll(List<SearchBookBean> newDataS, String keyWord)
    {
        List<SearchBookBean> copyDataS = new ArrayList<>(searchBooks);
        if (newDataS != null && newDataS.size() > 0)
        {
            saveData(newDataS);
            List<SearchBookBean> searchBookBeansAdd = new ArrayList<>();
            if (copyDataS.size() == 0)
            {
                for (SearchBookBean temp : newDataS)
                {
                    if (temp.getName().contains("没有找到你要的内容") || temp.getName().contains("进行深度搜索"))
                        newDataS.remove(temp);
                }
                copyDataS.addAll(newDataS);
            } else
            {
                //存在
                for (SearchBookBean temp : newDataS)
                {
                    if (temp.getName().contains("没有找到你要的内容"))
                        continue;
                    if (temp.getName().contains("进行深度搜索"))
                        continue;

                    if (TextUtils.isEmpty(temp.getName()) || TextUtils.isEmpty(temp.getAuthor()))
                        continue;

                    boolean hasSame = false;
                    for (int i = 0, size = copyDataS.size(); i < size; i++)
                    {
                        SearchBookBean searchBook = copyDataS.get(i);
                        if (TextUtils.equals(temp.getName(), searchBook.getName())
                                && TextUtils.equals(temp.getAuthor(), searchBook.getAuthor()))
                        {
                            hasSame = true;
                            searchBook.addOriginUrl(temp.getTag());
                            break;
                        }
                    }

                    if (!hasSame)
                    {
                        searchBookBeansAdd.add(temp);
                    }
                }
                //添加
                for (SearchBookBean temp : searchBookBeansAdd)
                {
                    if (TextUtils.equals(keyWord, temp.getName()))
                    {
                        for (int i = 0; i < copyDataS.size(); i++)
                        {
                            SearchBookBean searchBook = copyDataS.get(i);
                            if (!TextUtils.equals(keyWord, searchBook.getName()))
                            {
                                copyDataS.add(i, temp);
                                break;
                            }
                        }
                    } else if (TextUtils.equals(keyWord, temp.getAuthor()))
                    {
                        for (int i = 0; i < copyDataS.size(); i++)
                        {
                            SearchBookBean searchBook = copyDataS.get(i);
                            if (!TextUtils.equals(keyWord, searchBook.getName()) && !TextUtils.equals(keyWord, searchBook.getAuthor()))
                            {
                                copyDataS.add(i, temp);
                                break;
                            }
                        }
                    } else
                    {
                        copyDataS.add(temp);
                    }
                }
            }
            Collections.sort(copyDataS, new Comparator<SearchBookBean>()
            {
                @Override
                public int compare(SearchBookBean book1, SearchBookBean book2)
                {
                    if ((Levenshtein.getSimilarityRatio(book1.getName(), keyWord) - Levenshtein.getSimilarityRatio(book2.getName(), keyWord)) > 0)
                        return -1;
                    if ((Levenshtein.getSimilarityRatio(book1.getName(), keyWord) - Levenshtein.getSimilarityRatio(book2.getName(), keyWord) < 0))
                        return 1;

                    if ((Levenshtein.getSimilarityRatio(book1.getAuthor(), keyWord) - Levenshtein.getSimilarityRatio(book2.getAuthor(), keyWord)) > 0)
                        return -1;
                    if ((Levenshtein.getSimilarityRatio(book1.getAuthor(), keyWord) - Levenshtein.getSimilarityRatio(book2.getAuthor(), keyWord) < 0))
                        return 1;
                    return 0;
                }
            });
            Activity activity = activityRef.get();
            if (activity != null)
            {
                activity.runOnUiThread(() -> upData(DataAction.ADD, copyDataS));
            }
        }
    }

    private void saveData(List<SearchBookBean> data)
    {
        AsyncTask.execute(() -> DbHelper.getDaoSession().getSearchBookBeanDao().insertOrReplaceInTx(data));
    }

    public SearchBookBean getItemData(int pos)
    {
        return searchBooks.get(pos);
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        ViewGroup flContent;
        ImageView ivCover;
        TextView tvName;
        TextView tvState;
        TextView tvWords;
        TextView tvKind;
        TextView tvLasted;
        TextView tvOrigin;
        TextView tvOriginNum;
        TextView tvIntroduce;

        MyViewHolder(View itemView)
        {
            super(itemView);
            flContent = itemView.findViewById(R.id.fl_content);
            ivCover = itemView.findViewById(R.id.iv_cover);
            tvName = itemView.findViewById(R.id.tv_name);
            tvState = itemView.findViewById(R.id.tv_state);
            tvWords = itemView.findViewById(R.id.tv_words);
            tvLasted = itemView.findViewById(R.id.tv_lasted);
            tvKind = itemView.findViewById(R.id.tv_kind);
            tvOrigin = itemView.findViewById(R.id.tv_origin);
            tvOriginNum = itemView.findViewById(R.id.tv_origin_num);
            tvIntroduce = itemView.findViewById(R.id.tv_introduce);
        }
    }

    public enum DataAction
    {
        ADD, CLEAR
    }
}