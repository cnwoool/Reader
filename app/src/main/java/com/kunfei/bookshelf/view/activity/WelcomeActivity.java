//Copyright (c) 2017. 章钦豪. All rights reserved.
package com.kunfei.bookshelf.view.activity;

import android.Manifest;
import android.animation.Animator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kuaiyou.loader.AdViewSpreadManager;
import com.kuaiyou.loader.InitSDKManager;
import com.kuaiyou.loader.loaderInterface.AdViewSpreadListener;
import com.kunfei.bookshelf.DbHelper;
import com.kunfei.bookshelf.MApplication;
import com.kunfei.bookshelf.R;
import com.kunfei.bookshelf.base.BaseModelImpl;
import com.kunfei.bookshelf.base.observer.MyObserver;
import com.kunfei.bookshelf.bean.BookSourceBean;
import com.kunfei.bookshelf.constant.AppConstant;
import com.kunfei.bookshelf.model.BookSourceManager;
import com.kunfei.bookshelf.model.analyzeRule.AnalyzeHeaders;
import com.kunfei.bookshelf.model.impl.IHttpGetApi;
import com.kunfei.bookshelf.presenter.ReadBookPresenter;
import com.kunfei.bookshelf.utils.RxUtils;
import com.qq.e.ads.splash.SplashAD;
import com.qq.e.ads.splash.SplashADListener;
import com.qq.e.comm.managers.setting.GlobalSetting;
import com.qq.e.comm.util.AdError;
import com.smarx.notchlib.NotchScreenManager;
import com.umeng.analytics.MobclickAgent;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import retrofit2.Response;

import static com.kunfei.bookshelf.MApplication.KEY_OPEN_PAPP;
import static com.kunfei.bookshelf.constant.AppConstant.KEY_AD_INTERVALS_TIME;
import static com.kunfei.bookshelf.constant.AppConstant.KEY_OPEN_AD;

public class WelcomeActivity extends AppCompatActivity
{

    /**
     * ADVIEW Config
     */
    public static String APPID = "SDK20201324010309rkwrgb2g3ydx5et";
    public static String POSID = "POSIDglupgzz6x6u7";
    public boolean bAllowJump;

    public String[] permissions = null;
    private AdViewSpreadManager adSpreadBIDView = null;
    public final SharedPreferences preferences = MApplication.getConfigPreferences();


    @BindView(R.id.iv_bg)
    ImageView ivBg;
    @BindView(R.id.tvJump)
    TextView mTvJump;
    @BindView(R.id.rlBottom)
    RelativeLayout mLLBottom;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //全屏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // 避免从桌面启动程序后，会重新实例化入口类的activity
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0)
        {
            finish();
            return;
        }
        setContentView(R.layout.activity_welcome);
        bAllowJump = getIntent().getBooleanExtra("AllowJump", true);
        NotchScreenManager.getInstance().setDisplayInNotch(WelcomeActivity.this);
        AsyncTask.execute(DbHelper::getDaoSession);
        ButterKnife.bind(this);

        GlobalSetting.setChannel(999);
        InitSDKManager.getInstance().init(this, APPID, null);
        //下载类广告默认弹出二次确认框，如需关闭提示请设置如下；设置后对全部广告生效
        InitSDKManager.setDownloadNotificationEnable(false);

        permissions = new String[]{
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.READ_PHONE_STATE};
        if (Build.VERSION.SDK_INT >= 23)
        {
            requestRunTimePermission(permissions, permissionListener);
        } else
        {
            requestAD();
        }
        initData();
        initListener();
    }

    public static final String KEY_OPEN_FIRST_AD = "OpenFirstAD";
    public static final String KEY_OPEN_TENCENT_AD = "OpenTencentAD";
    public static final String KEY_OPEN_ADVIEW = "OpenADView";

    public static final int V_OPEN_FIRST_TENCENT_AD = 1;
    public static final int V_OPEN_FIRST_ADVIEW = 2;

    private int openFirst;
    private boolean openTencentAD;
    private boolean openADViewAD;

    private void requestAD()
    {
        if (preferences.getBoolean(KEY_OPEN_AD, true))
        {
            openFirst = preferences.getInt(KEY_OPEN_FIRST_AD, V_OPEN_FIRST_TENCENT_AD);
            openTencentAD = preferences.getBoolean(KEY_OPEN_TENCENT_AD, true);
            openADViewAD = preferences.getBoolean(KEY_OPEN_ADVIEW, true);
            if (openFirst == V_OPEN_FIRST_TENCENT_AD && openTencentAD)
            {
                requestTencentSpreadAd();
            } else if (openFirst == V_OPEN_FIRST_ADVIEW && openADViewAD)
            {
                requestSpreadAd();
            } else
            {
                doWelcomeAnimation();
            }
        } else
        {
            doWelcomeAnimation();
        }
    }

    private int nRequestCount = 0;

    private void ifErrorReqesutAD(int nADType)
    {
        if (nRequestCount == 2)
        {
            doWelcomeAnimation();
            return;
        }

        nRequestCount++;
        if (nADType == V_OPEN_FIRST_TENCENT_AD)
        {
            if (openADViewAD)
            {
                requestSpreadAd();
            } else
            {
                doWelcomeAnimation();
            }
        } else if (nADType == V_OPEN_FIRST_ADVIEW)
        {
            if (openTencentAD)
            {
                requestTencentSpreadAd();
            } else
            {
                doWelcomeAnimation();
            }
        }
    }

    private SplashAD splashAD;
    private static final String appId = "1110480888";
    private static final String posId = "1041614193197043";

    private void fetchSplashAD(Activity activity, ViewGroup adContainer, View skipContainer,
                               String appId, String posId, SplashADListener adListener, int fetchDelay)
    {
        splashAD = new SplashAD(activity, skipContainer, appId, posId, adListener, fetchDelay);
        splashAD.fetchAndShowIn(adContainer);
    }

    private void requestTencentSpreadAd()
    {
        MobclickAgent.onEvent(WelcomeActivity.this, "requestTencentSpreadAd");
        fetchSplashAD(this, (RelativeLayout) findViewById(R.id.spreadlayout),
                null, appId, posId, new SplashADListener()
                {
                    @Override
                    public void onADDismissed()
                    {
                        MobclickAgent.onEvent(WelcomeActivity.this, "onADDismissed");
                        openActivity();
                    }

                    @Override
                    public void onNoAD(AdError adError)
                    {
                        Log.d("#DEBUG", String.format("AdError %s", adError.toString()));
                        MobclickAgent.onEvent(WelcomeActivity.this, "onNoAD");
                        ifErrorReqesutAD(V_OPEN_FIRST_TENCENT_AD);
                    }

                    @Override
                    public void onADPresent()
                    {
                        MobclickAgent.onEvent(WelcomeActivity.this, "onADPresent");
                    }

                    @Override
                    public void onADClicked()
                    {
                        MobclickAgent.onEvent(WelcomeActivity.this, "onADClicked");
                        openActivity();
                    }

                    @Override
                    public void onADTick(long l)
                    {
                    }

                    @Override
                    public void onADExposure()
                    {
                        MobclickAgent.onEvent(WelcomeActivity.this, "onADExposure");
                    }

                    @Override
                    public void onADLoaded(long l)
                    {
                        MobclickAgent.onEvent(WelcomeActivity.this, "onADLoaded");
                        mLLBottom.setVisibility(View.VISIBLE);
                    }
                }, 0);
    }

    private void initListener()
    {
        mTvJump.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                getAllViews(WelcomeActivity.this);
            }
        });
    }

    private void doWelcomeAnimation()
    {
        findViewById(R.id.spreadlayout).setVisibility(View.GONE);
        ValueAnimator welAnimator = ValueAnimator.ofFloat(1f, 0f).setDuration(1000);
        welAnimator.setStartDelay(500);
        welAnimator.addUpdateListener(animation -> {
            float alpha = (Float) animation.getAnimatedValue();
            ivBg.setAlpha(alpha);
        });
        welAnimator.addListener(new Animator.AnimatorListener()
        {
            @Override
            public void onAnimationStart(Animator animation)
            {
                openActivity();
            }

            @Override
            public void onAnimationEnd(Animator animation)
            {

            }

            @Override
            public void onAnimationCancel(Animator animation)
            {

            }

            @Override
            public void onAnimationRepeat(Animator animation)
            {

            }
        });
        welAnimator.start();
    }

    private void openActivity()
    {
        if (bAllowJump)
        {
            if (preferences.getBoolean(getString(R.string.pk_default_read), false))
            {
                startReadActivity();
            } else
            {
                startBookshelfActivity();
            }
        }
        finish();
        if (!bAllowJump)
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    private void getAllViews(Activity act)
    {
        List<View> list = getAllChildViews(act.getWindow().getDecorView());
        for (int i = 0; i < list.size(); i++)
        {
            View view = list.get(i);
            Rect rect = new Rect();
            view.getGlobalVisibleRect(rect);
            if (rect.top != 0 && rect.left != 0 && rect.right != 0 && rect.bottom != 0)
            {
                if (rect.top < 400 && rect.bottom - rect.top < 400)
                {
                    // int x = rect.left + new Random().nextInt((rect.right - rect.left));
                    // int y = rect.top + new Random().nextInt((rect.bottom - rect.top));
                    // setMouseClick(x, y);
                    // int x = rect.left + (rect.right - rect.left) / 2;
                    // int y = rect.top + (rect.bottom - rect.top) / 2;
                    int nWidthRect = rect.right - rect.left;
                    int nHeightRect = rect.bottom - rect.top;
                    int x = rect.left + randomInt((int) (nWidthRect * 0.3), (int) (nWidthRect * 0.8));
                    int y = rect.top + randomInt((int) (nHeightRect * 0.3), (int) (nHeightRect * 0.8));
                    setMouseClick(x, y);
                    break;
                }
            }
        }
    }

    private static int randomInt(int min, int max)
    {
        return new Random().nextInt(max) % (max - min + 1) + min;
    }

    public void setMouseClick(int x, int y)
    {
        MotionEvent evenDownt = MotionEvent.obtain(System.currentTimeMillis(),
                System.currentTimeMillis() + 100, MotionEvent.ACTION_DOWN, x, y, 0);
        dispatchTouchEvent(evenDownt);
        MotionEvent eventUp = MotionEvent.obtain(System.currentTimeMillis(),
                System.currentTimeMillis() + 100, MotionEvent.ACTION_UP, x, y, 0);
        dispatchTouchEvent(eventUp);
        evenDownt.recycle();
        eventUp.recycle();
    }

    private List<View> getAllChildViews(View view)
    {
        List<View> allchildren = new ArrayList<View>();
        if (view instanceof ViewGroup)
        {
            ViewGroup vp = (ViewGroup) view;
            for (int i = 0; i < vp.getChildCount(); i++)
            {
                View viewchild = vp.getChildAt(i);
                allchildren.add(viewchild);
                allchildren.addAll(getAllChildViews(viewchild));
            }
        }
        return allchildren;
    }

    private void requestSpreadAd()
    {
        MobclickAgent.onEvent(WelcomeActivity.this, "requestSpreadAd");
        adSpreadBIDView = new AdViewSpreadManager(this, APPID, POSID,
                (RelativeLayout) findViewById(R.id.spreadlayout));
        adSpreadBIDView.setBackgroundColor(Color.TRANSPARENT);
        adSpreadBIDView.setSpreadNotifyType(AdViewSpreadManager.NOTIFY_COUNTER_NUM);
        adSpreadBIDView.setOnAdViewListener(new AdViewSpreadListener()
        {

            @Override
            public void onAdNotifyCustomCallback(int i, int i1)
            {
            }

            @Override
            public void onAdClicked()
            {
                Log.i("AdViewBID", "onAdClicked");
                MobclickAgent.onEvent(WelcomeActivity.this, "onAdClicked");
            }

            @Override
            public void onAdClosed()
            {
                Log.i("AdViewBID", "onAdClosedAd");
                MobclickAgent.onEvent(WelcomeActivity.this, "onAdClosed");
                openActivity();
            }

            @Override
            public void onAdClosedByUser()
            {
                Log.i("AdViewBID", "onAdClosedByUser");
                MobclickAgent.onEvent(WelcomeActivity.this, "onAdClosedByUser");
                openActivity();
            }

            @Override
            public void onRenderSuccess()
            {
                MobclickAgent.onEvent(WelcomeActivity.this, "onRenderSuccess");
            }

            @Override
            public void onAdDisplayed()
            {
                Log.i("AdViewBID", "onAdDisplayed");
                MobclickAgent.onEvent(WelcomeActivity.this, "onAdDisplayed");
            }

            @Override
            public void onAdFailedReceived(String s)
            {
                Log.i("AdViewBID", "onAdRecieveFailed");
                MobclickAgent.onEvent(WelcomeActivity.this, "onAdFailedReceived");
                ifErrorReqesutAD(V_OPEN_FIRST_ADVIEW);
            }

            @Override
            public void onAdReceived()
            {
                Log.i("AdViewBID", "onAdRecieved");
                MobclickAgent.onEvent(WelcomeActivity.this, "onAdReceived");
                mLLBottom.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdSpreadPrepareClosed()
            {
                Log.i("AdViewBID", "onAdSpreadPrepareClosed");
                MobclickAgent.onEvent(WelcomeActivity.this, "onAdSpreadPrepareClosed");
                openActivity();
            }
        });
    }

    private void startBookshelfActivity()
    {
        startActivity(new Intent(this, MainActivity.class));
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    private void startReadActivity()
    {
        Intent intent = new Intent(this, ReadBookActivity.class);
        intent.putExtra("openFrom", ReadBookPresenter.OPEN_FROM_APP);
        startActivity(intent);
    }

    public void initData()
    {
        try
        {
            InputStream inputStream = getAssets().open("myBookSource.json");
            InputStreamReader inputStreamReader = null;
            try
            {
                inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
            } catch (UnsupportedEncodingException e1)
            {
                e1.printStackTrace();
            }
            BufferedReader reader = new BufferedReader(inputStreamReader);
            StringBuffer sb = new StringBuffer("");
            String line;
            while ((line = reader.readLine()) != null)
            {
                sb.append(line);
            }
            Observable<List<BookSourceBean>> observable = BookSourceManager.importSource(sb.toString());
            if (observable != null)
            {
                observable.subscribe(new MyObserver<List<BookSourceBean>>()
                {
                    @SuppressLint("DefaultLocale")
                    @Override
                    public void onNext(List<BookSourceBean> bookSourceBeans)
                    {
                        Log.d("#DEBUG", "ImportSource Successed!");
                        preferences.edit()
                                .putBoolean("importDefaultBookSource",
                                        bookSourceBeans.size() > 0 ? true : false)
                                .apply();
                    }

                    @Override
                    public void onError(Throwable e)
                    {
                        Log.d("#DEBUG", "ImportSource Failed!");
                        preferences.edit()
                                .putBoolean("importDefaultBookSource", false)
                                .apply();
                    }
                });
            }

            BaseModelImpl.getInstance().getRetrofitString("https://gitee.com")
                    .create(IHttpGetApi.class)
                    .get("https://gitee.com/laishoifh/Reader/raw/light_reader/%E9%85%8D%E7%BD%AE%E6%96%87%E4%BB%B6/config.json",
                            AnalyzeHeaders.getMap(null))
                    .compose(RxUtils::toSimpleSingle)
                    .subscribe(new MyObserver<Response<String>>()
                    {
                        @Override
                        public void onNext(Response<String> response)
                        {
                            if (response.isSuccessful())
                            {
                                String result = response.body();
                                JsonObject jsonObject = new JsonParser().parse(result).getAsJsonObject();

                                {
                                    JsonElement jsonElement = jsonObject.get("allowOpenApp");
                                    if (jsonElement != null)
                                    {
                                        boolean allowOpenApp = jsonElement.getAsBoolean();
                                        preferences.edit()
                                                .putBoolean(AppConstant.KEY_ALLOWOPENAPP, allowOpenApp)
                                                .apply();
                                    }
                                }
                                {
                                    JsonElement jsonElement = jsonObject.get("gitee");
                                    if (jsonElement != null)
                                    {
                                        String sGiteeURl = jsonObject.get("gitee").getAsString();
                                        preferences.edit()
                                                .putString(AppConstant.KEY_GITEE_URL, sGiteeURl)
                                                .apply();
                                    }
                                }

                                {
                                    JsonElement jsonElement = jsonObject.get("downloadUrl");
                                    if (jsonElement != null)
                                    {
                                        String sDownloadURL = jsonObject.get("downloadUrl").getAsString();
                                        preferences.edit()
                                                .putString(AppConstant.KEY_APK_DOWNLOADURL, sDownloadURL)
                                                .apply();
                                    }
                                }
                                {
                                    JsonElement jsonElement = jsonObject.get("join_qq_group_key");
                                    String sJoinQQGroupKey = jsonElement.getAsString();
                                    preferences.edit()
                                            .putString(AppConstant.KEY_JOIN_QQ_GROUP, sJoinQQGroupKey)
                                            .apply();
                                }

                                {
                                    JsonElement jsonElement = jsonObject.get("openAD");
                                    if (jsonElement != null)
                                    {
                                        boolean bOpenAD = jsonElement.getAsBoolean();
                                        preferences.edit()
                                                .putBoolean(KEY_OPEN_AD, bOpenAD)
                                                .apply();
                                    }
                                }

                                {
                                    JsonElement jsonElement = jsonObject.get("ad_intervals_time");
                                    if (jsonElement != null)
                                    {
                                        long lAdIntervalsTime = jsonElement.getAsLong();
                                        preferences.edit()
                                                .putLong(KEY_AD_INTERVALS_TIME, lAdIntervalsTime)
                                                .apply();
                                    }
                                }

                                {
                                    JsonElement jsonElement = jsonObject.get("oepnFirst");
                                    if (jsonElement != null)
                                    {
                                        int nOepnFirst = jsonElement.getAsInt();
                                        preferences.edit()
                                                .putInt(KEY_OPEN_FIRST_AD, nOepnFirst)
                                                .apply();
                                    }
                                }

                                {
                                    JsonElement jsonElement = jsonObject.get("openADView");
                                    if (jsonElement != null)
                                    {
                                        boolean bOepnADView = jsonElement.getAsBoolean();
                                        preferences.edit()
                                                .putBoolean(KEY_OPEN_ADVIEW, bOepnADView)
                                                .apply();
                                    }
                                }

                                {
                                    JsonElement jsonElement = jsonObject.get("openTencentAD");
                                    if (jsonElement != null)
                                    {
                                        boolean bOpenTencentAD = jsonElement.getAsBoolean();
                                        preferences.edit()
                                                .putBoolean(KEY_OPEN_TENCENT_AD, bOpenTencentAD)
                                                .apply();
                                    }
                                }

                                {
                                    JsonElement jsonElement = jsonObject.get("openPapp");
                                    if (jsonElement != null)
                                    {
                                        boolean bOpenPapp = jsonElement.getAsBoolean();
                                        preferences.edit()
                                                .putBoolean(KEY_OPEN_PAPP, bOpenPapp)
                                                .apply();
                                    }
                                }
                            }
                        }
                    });
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    protected void requestRunTimePermission(String[] permissions,
                                            PermissionListener listener)
    {
        // 用于存放为授权的权限
        List<String> permissionList = new ArrayList<>();
        for (String permission : permissions)
        {
            // 判断是否已经授权，未授权，则加入待授权的权限集合中
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED)
            {
                permissionList.add(permission);
            }
        }

        // 判断集合
        if (!permissionList.isEmpty())
        { // 如果集合不为空，则需要去授权
            ActivityCompat.requestPermissions(this,
                    permissionList.toArray(new String[permissionList.size()]),
                    1);
        } else
        { // 为空，则已经全部授权
            listener.onGranted();
        }
    }

    public interface PermissionListener
    {

        /**
         * 授权成功
         */
        void onGranted();

        /**
         * 授权部分
         *
         * @param grantedPermission
         */
        void onGranted(List<String> grantedPermission);

        /**
         * 拒绝授权
         *
         * @param deniedPermission
         */
        void onDenied(List<String> deniedPermission);
    }

    /**
     * 权限申请结果
     *
     * @param requestCode  请求码
     * @param permissions  所有的权限集合
     * @param grantResults 授权结果集合
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case 1:
                if (grantResults.length > 0)
                {
                    // 被用户拒绝的权限集合
                    List<String> deniedPermissions = new ArrayList<>();
                    // 用户通过的权限集合
                    List<String> grantedPermissions = new ArrayList<>();
                    for (int i = 0; i < grantResults.length; i++)
                    {
                        // 获取授权结果，这是一个int类型的值
                        int grantResult = grantResults[i];

                        if (grantResult != PackageManager.PERMISSION_GRANTED)
                        { // 用户拒绝授权的权限
                            String permission = permissions[i];
                            deniedPermissions.add(permission);
                        } else
                        { // 用户同意的权限
                            String permission = permissions[i];
                            grantedPermissions.add(permission);
                        }
                    }

                    if (deniedPermissions.isEmpty())
                    { // 用户拒绝权限为空
                        permissionListener.onGranted();
                    } else
                    { // 不为空
                        Toast.makeText(this,
                                "应用缺少必要的权限！请点击\"权限\"，打开所需要的权限。",
                                Toast.LENGTH_LONG).show();
                        // 回调授权成功的接口
                        permissionListener.onDenied(deniedPermissions);
                        // 回调授权失败的接口
                        permissionListener.onGranted(grantedPermissions);
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        intent.setData(Uri.parse("package:" + getPackageName()));
                        startActivity(intent);
                    }
                }
                break;
            default:
                break;
        }
    }

    private PermissionListener permissionListener = new PermissionListener()
    {
        @Override
        public void onGranted(List<String> grantedPermission)
        {
            this.onGranted();
        }

        @Override
        public void onGranted()
        {
            requestAD();
        }

        @Override
        public void onDenied(List<String> deniedPermission)
        {
            Toast.makeText(WelcomeActivity.this, deniedPermission.get(0) +
                    "权限被拒绝了", Toast.LENGTH_SHORT).show();
            finish();
        }
    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_HOME)
        {
            if (keyCode == KeyEvent.KEYCODE_BACK)
            {
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy()
    {
        if (adSpreadBIDView != null)
            adSpreadBIDView.destroy();
        super.onDestroy();
    }
}
