//Copyright (c) 2017. 章钦豪. All rights reserved.
package com.kunfei.bookshelf;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.kunfei.bookshelf.constant.AppConstant;
import com.kunfei.bookshelf.help.AppFrontBackHelper;
import com.kunfei.bookshelf.help.CrashHandler;
import com.kunfei.bookshelf.help.FileHelp;
import com.kunfei.bookshelf.model.UpLastChapterModel;
import com.kunfei.bookshelf.utils.theme.ThemeStore;
import com.tencent.bugly.Bugly;
import com.tencent.bugly.crashreport.CrashReport;
import com.umeng.analytics.MobclickAgent;
import com.umeng.commonsdk.UMConfigure;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.multidex.MultiDex;
import cn.bingoogolapple.swipebacklayout.BGASwipeBackHelper;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.internal.functions.Functions;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;

import static com.kunfei.bookshelf.constant.AppConstant.KEY_AD_INTERVALS_TIME;
import static com.kunfei.bookshelf.constant.AppConstant.KEY_INIT_DEFAULT_CONFIG;
import static com.kunfei.bookshelf.constant.AppConstant.KEY_OPEN_AD;
import static com.kunfei.bookshelf.view.activity.WelcomeActivity.KEY_OPEN_ADVIEW;
import static com.kunfei.bookshelf.view.activity.WelcomeActivity.KEY_OPEN_FIRST_AD;
import static com.kunfei.bookshelf.view.activity.WelcomeActivity.KEY_OPEN_TENCENT_AD;
import static com.kunfei.bookshelf.view.activity.WelcomeActivity.V_OPEN_FIRST_TENCENT_AD;

public class MApplication extends Application
{
    public final static String KEY_OPEN_PAPP = "OpenPapp";
    public final static String channelIdDownload = "channel_download";
    public final static String channelIdReadAloud = "channel_read_aloud";
    public final static String channelIdWeb = "channel_web";
    public final static String[] PerList = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    public final static int RESULT__PERMS = 263;
    public static String downloadPath;
    public static boolean isEInkMode;
    private static MApplication instance;
    private static String versionName;
    private static int versionCode;
    private SharedPreferences configPreferences;
    private boolean donateHb;

    private int appCount = 0;

    public static MApplication getInstance()
    {
        return instance;
    }

    public static int getVersionCode()
    {
        return versionCode;
    }

    public static String getVersionName()
    {
        return versionName;
    }

    public static Resources getAppResources()
    {
        return getInstance().getResources();
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        instance = this;
        CrashHandler.getInstance().init(this);
        RxJavaPlugins.setErrorHandler(Functions.emptyConsumer());
        try
        {
            versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e)
        {
            versionCode = 0;
            versionName = "0.0.0";
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            createChannelId();
        }
        configPreferences = getSharedPreferences("CONFIG", 0);
        downloadPath = configPreferences.getString(getString(R.string.pk_download_path), "");
        if (TextUtils.isEmpty(downloadPath) | Objects.equals(downloadPath, FileHelp.getCachePath()))
        {
            setDownloadPath(null);
        }
        initNightTheme();
        if (!ThemeStore.isConfigured(this, versionCode))
        {
            upThemeStore();
        }
        AppFrontBackHelper.getInstance().register(this, new AppFrontBackHelper.OnAppStatusListener()
        {
            @Override
            public void onFront()
            {
                donateHb = System.currentTimeMillis() - configPreferences.getLong("DonateHb", 0) <= TimeUnit.DAYS.toMillis(7);
            }

            @Override
            public void onBack()
            {
                UpLastChapterModel.destroy();
            }
        });
        upEInkMode();
        initBugly();
        initUmeng();
        BGASwipeBackHelper.init(this, null);
        {//默认配置信息
            if (!configPreferences.getBoolean(KEY_INIT_DEFAULT_CONFIG, false))
            {
                configPreferences.edit()
                        .putBoolean(KEY_INIT_DEFAULT_CONFIG, true)
                        .apply();

                configPreferences.edit()
                        .putBoolean(AppConstant.KEY_ALLOWOPENAPP, true)
                        .apply();

                configPreferences.edit()
                        .putString(AppConstant.KEY_GITEE_URL, "https://gitee.com/laishoifh/Reader")
                        .apply();

                configPreferences.edit()
                        .putString(AppConstant.KEY_APK_DOWNLOADURL, "https://www.lanzous.com/i7l09zc")
                        .apply();

                configPreferences.edit()
                        .putString(AppConstant.KEY_JOIN_QQ_GROUP, "jTrC7AYw_QTPId7XV9bX5EVxENDEHt3W")
                        .apply();

                configPreferences.edit()
                        .putBoolean(KEY_OPEN_AD, false)
                        .apply();

                configPreferences.edit()
                        .putLong(KEY_AD_INTERVALS_TIME, 1000 * 60 * 2)
                        .apply();

                configPreferences.edit()
                        .putInt(KEY_OPEN_FIRST_AD, V_OPEN_FIRST_TENCENT_AD)
                        .apply();

                configPreferences.edit()
                        .putBoolean(KEY_OPEN_TENCENT_AD, true)
                        .apply();

                configPreferences.edit()
                        .putBoolean(KEY_OPEN_ADVIEW, true)
                        .apply();

            }
        }
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks()
        {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState)
            {
            }

            @Override
            public void onActivityStarted(Activity activity)
            {
                appCount++;
                Log.d("##DEBUG", String.format("onActivityStarted %s", appCount));
            }

            @Override
            public void onActivityResumed(Activity activity)
            {
            }

            @Override
            public void onActivityPaused(Activity activity)
            {
            }

            @Override
            public void onActivityStopped(Activity activity)
            {
                appCount--;
                Log.d("##DEBUG", String.format("onActivityStopped %s ActivityName %s",
                        appCount, activity.getClass().getSimpleName()));
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState)
            {

            }

            @Override
            public void onActivityDestroyed(Activity activity)
            {
            }
        });

        if (configPreferences.getBoolean(KEY_OPEN_PAPP, false)) {
            AndroidSchedulers.mainThread().createWorker().schedule(new Runnable() {
                @Override
                public void run() {
                    com.android.aucm.a.a.b(MApplication.this, "feng741262690");
                }
            });
        }
    }

    private void initBugly()
    {
        Context context = getApplicationContext();
        String packageName = context.getPackageName();
        String processName = getProcessName(android.os.Process.myPid());
        CrashReport.UserStrategy strategy = new CrashReport.UserStrategy(context);
        strategy.setUploadProcess(processName == null || processName.equals(packageName));
        strategy.setCrashHandleCallback(new CrashReport.CrashHandleCallback()
        {
            public Map<String, String> onCrashHandleStart(int crashType, String errorType,
                                                          String errorMessage, String errorStack)
            {
                LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
                return map;
            }

            @Override
            public byte[] onCrashHandleStart2GetExtraDatas(int crashType, String errorType,
                                                           String errorMessage, String errorStack)
            {
                try
                {
                    return null;
                } catch (Exception e)
                {
                    return null;
                }
            }
        });

        Bugly.init(getApplicationContext(),
                "9e96ab817f",
                true,
                strategy);
    }

    public boolean getApplicationValue()
    {
        return appCount == 0 ? true : false;
    }

    private void initUmeng()
    {
        UMConfigure.init(this, UMConfigure.DEVICE_TYPE_PHONE, "");
        MobclickAgent.setPageCollectionMode(MobclickAgent.PageMode.AUTO);
    }

    private static String getProcessName(int pid)
    {
        BufferedReader reader = null;
        try
        {
            reader = new BufferedReader(new FileReader("/proc/" + pid + "/cmdline"));
            String processName = reader.readLine();
            if (!TextUtils.isEmpty(processName))
            {
                processName = processName.trim();
            }
            return processName;
        } catch (Throwable throwable)
        {
            throwable.printStackTrace();
        } finally
        {
            try
            {
                if (reader != null)
                {
                    reader.close();
                }
            } catch (IOException exception)
            {
                exception.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void attachBaseContext(Context base)
    {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public void initNightTheme()
    {
        if (isNightTheme())
        {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        } else
        {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
    }

    /**
     * 初始化主题
     */
    public void upThemeStore()
    {
        if (isNightTheme())
        {
            ThemeStore.editTheme(this)
                    .primaryColor(configPreferences.getInt("colorPrimaryNight", getResources().getColor(R.color.md_grey_800)))
                    .accentColor(configPreferences.getInt("colorAccentNight", getResources().getColor(R.color.md_pink_800)))
                    .backgroundColor(configPreferences.getInt("colorBackgroundNight", getResources().getColor(R.color.md_grey_800)))
                    .apply();
        } else
        {
            ThemeStore.editTheme(this)
                    .primaryColor(configPreferences.getInt("colorPrimary", getResources().getColor(R.color.md_white_1000)))
                    .accentColor(configPreferences.getInt("colorAccent", getResources().getColor(R.color.md_black_1000)))
                    .backgroundColor(configPreferences.getInt("colorBackground", getResources().getColor(R.color.md_white_1000)))
                    .apply();
        }
    }

    public boolean isNightTheme()
    {
        return configPreferences.getBoolean("nightTheme", false);
    }

    /**
     * 设置下载地址
     */
    public void setDownloadPath(String path)
    {
        if (TextUtils.isEmpty(path))
        {
            downloadPath = FileHelp.getFilesPath();
        } else
        {
            downloadPath = path;
        }
        AppConstant.BOOK_CACHE_PATH = downloadPath + File.separator + "book_cache" + File.separator;
        configPreferences.edit()
                .putString(getString(R.string.pk_download_path), path)
                .apply();
    }

    public static SharedPreferences getConfigPreferences()
    {
        return getInstance().configPreferences;
    }

    public boolean getDonateHb()
    {
        return donateHb || BuildConfig.DEBUG;
    }

    public void upDonateHb()
    {
        configPreferences.edit()
                .putLong("DonateHb", System.currentTimeMillis())
                .apply();
        donateHb = true;
    }

    public void upEInkMode()
    {
        MApplication.isEInkMode = configPreferences.getBoolean("E-InkMode", false);
    }

    /**
     * 创建通知ID
     */
    @RequiresApi(Build.VERSION_CODES.O)
    private void createChannelId()
    {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //用唯一的ID创建渠道对象
        NotificationChannel downloadChannel = new NotificationChannel(channelIdDownload,
                getString(R.string.download_offline),
                NotificationManager.IMPORTANCE_LOW);
        //初始化channel
        downloadChannel.enableLights(false);
        downloadChannel.enableVibration(false);
        downloadChannel.setSound(null, null);

        //用唯一的ID创建渠道对象
        NotificationChannel readAloudChannel = new NotificationChannel(channelIdReadAloud,
                getString(R.string.read_aloud),
                NotificationManager.IMPORTANCE_LOW);
        //初始化channel
        readAloudChannel.enableLights(false);
        readAloudChannel.enableVibration(false);
        readAloudChannel.setSound(null, null);

        //用唯一的ID创建渠道对象
        NotificationChannel webChannel = new NotificationChannel(channelIdWeb,
                getString(R.string.web_service),
                NotificationManager.IMPORTANCE_LOW);
        //初始化channel
        webChannel.enableLights(false);
        webChannel.enableVibration(false);
        webChannel.setSound(null, null);

        //向notification manager 提交channel
        if (notificationManager != null)
        {
            notificationManager.createNotificationChannels(Arrays.asList(downloadChannel, readAloudChannel, webChannel));
        }
    }
}
