package com.kunfei.bookshelf.presenter;

import com.kunfei.basemvplib.BasePresenterImpl;
import com.kunfei.bookshelf.base.observer.MyObserver;
import com.kunfei.bookshelf.bean.SearchBookBean;
import com.kunfei.bookshelf.help.BookRackHelpter;
import com.kunfei.bookshelf.presenter.contract.SubCategoryContract;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SubCategoryPresenter extends BasePresenterImpl<SubCategoryContract.View> implements SubCategoryContract.Presenter
{
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    @Override
    public void getBookList(String strGender, String strType, String strMajor, String strMinor, int nStart, int nLimits)
    {
        BookRackHelpter.getInstance().getBooksByCats(strGender, strType, strMajor, strMinor, nStart, nLimits)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new MyObserver<List<SearchBookBean>>()
        {
            @Override
            public void onNext(List<SearchBookBean> bean)
            {
                if (mView != null)
                {
                    mView.showBookList(bean);
                }
            }

            @Override
            public void onSubscribe(Disposable d)
            {
                mCompositeDisposable.add(d);
            }

            @Override
            public void onError(Throwable e)
            {
            }
        });
    }

    @Override
    public void detachView()
    {
        if (mCompositeDisposable != null)
        {
            mCompositeDisposable.dispose();
        }
    }
}
