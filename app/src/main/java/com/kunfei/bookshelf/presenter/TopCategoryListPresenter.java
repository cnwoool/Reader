package com.kunfei.bookshelf.presenter;

import com.kunfei.basemvplib.BasePresenterImpl;
import com.kunfei.bookshelf.base.observer.MyObserver;
import com.kunfei.bookshelf.bean.CategoryListBean;
import com.kunfei.bookshelf.help.BookRackHelpter;
import com.kunfei.bookshelf.presenter.contract.TopCategoryListContract;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class TopCategoryListPresenter extends BasePresenterImpl<TopCategoryListContract.View> implements TopCategoryListContract.Presenter
{

    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    @Override
    public void detachView()
    {
        if (mCompositeDisposable != null)
        {
            mCompositeDisposable.dispose();
            mCompositeDisposable = null;
        }
    }

    @Override
    public void getCategoryList()
    {
        BookRackHelpter.getInstance().getCategoryList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new MyObserver<CategoryListBean>()
        {

            @Override
            public void onSubscribe(Disposable d)
            {
                mCompositeDisposable.add(d);
            }

            @Override
            public void onNext(CategoryListBean bean)
            {
                if (mView != null)
                {
                    mView.showCategoryList(bean);
                }
            }
        });
    }
}
