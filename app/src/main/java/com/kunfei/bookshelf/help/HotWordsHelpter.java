package com.kunfei.bookshelf.help;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kunfei.bookshelf.base.BaseModelImpl;
import com.kunfei.bookshelf.model.analyzeRule.AnalyzeHeaders;
import com.kunfei.bookshelf.model.impl.IHttpGetApi;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

public class HotWordsHelpter
{

    private static HotWordsHelpter mHelpter;

    public static HotWordsHelpter getInstance()
    {
        if (mHelpter == null)
        {
            synchronized (HotWordsHelpter.class)
            {
                if (mHelpter == null)
                {
                    mHelpter = new HotWordsHelpter();
                }
            }
        }
        return mHelpter;
    }

    public Observable<List<String>> getHotWords()
    {
        return BaseModelImpl.getInstance().getRetrofitString("http://api.zhuishushenqi.com")
                .create(IHttpGetApi.class)
                .get("http://api.zhuishushenqi.com/book/hot-word", AnalyzeHeaders.getMap(null))
                .flatMap(response -> analyzeLastReleaseApi(response.body()));
    }

    public Observable<List<String>> analyzeLastReleaseApi(final String jsonStr)
    {
        return Observable.create(e -> {
            List<String> hotWords = new ArrayList<>();
            JsonObject jsonObject = new JsonParser().parse(jsonStr).getAsJsonObject();
            if (jsonObject.has("ok") && jsonObject.get("ok").getAsBoolean())
            {
                if (jsonObject.has("hotWords"))
                {
                    JsonArray jsonArray = jsonObject.getAsJsonArray("hotWords");
                    for (int i = 0; i < jsonArray.size(); i++)
                    {
                        hotWords.add(jsonArray.get(i).getAsString());
                    }
                }
            }
            e.onNext(hotWords);
            e.onComplete();
        });
    }
}
