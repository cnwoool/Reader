package com.kunfei.bookshelf.help;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.kunfei.bookshelf.MApplication;
import com.kunfei.bookshelf.R;
import com.kunfei.bookshelf.utils.DensityUtil;

import androidx.annotation.DrawableRes;

public class GlideUtil
{
    private static RoundedCorners roundedCorners = new RoundedCorners(DensityUtil.dp2px(MApplication.getInstance(), 5));
    private static RequestOptions options = RequestOptions.bitmapTransform(roundedCorners);

    private static RequestBuilder<Drawable> loadTransform(Context context, @DrawableRes int placeholderId, float radius)
    {
        RoundedCorners roundedCorners = new RoundedCorners(DensityUtil.dp2px(context, radius));
        RequestOptions options = RequestOptions.bitmapTransform(roundedCorners);
        return Glide.with(context)
                .load(placeholderId)
                .apply(options);
    }

    public static void loadCircleImage(Context context, ImageView ivCover, String url)
    {
        Glide.with(context)
                .load(url)
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .apply(options)
                .thumbnail(loadTransform(context, R.drawable.img_cover_default, 5))
                .into(ivCover);
    }
}
